// index.js

class Cart {

	constructor(){

		this.contents = [];
		this.totalAmount = 0;

	}

	addToCart(product,quantity){

		let order = {};
		order["product"] = product;
		order["quantity"] = quantity;
		this.contents.push(order);
		return this
		
	}

	showCartContents(){

		console.log(this.contents);
		return this;
	}

	clearCartContents(){

		this.contents = [];
		this.totalAmount = 0;
		return this
	}

	computeTotal(){

		let sum = 0;
		let i = 0;
		this.contents.forEach(order => {
			sum += order.product.price * order.quantity
			if(i<this.contents.length){
				i++
			};				
		})

		this.totalAmount = sum;
		return this;

	}
}


class Product {

	constructor(name,price){

		this.name = name;
		this.price = price;
		this.isActive = true;

	}

	updatePrice(price){

		this.price = price;
		return this;

	}

	archive(){

		this.isActive = false;
		return this;

	}
}

class Customer {

	constructor(email){

		this.email = email;
		this.cart = new Cart("Cart1");
		this.orders = [];
	}

	checkOut(){

		let orders = [];
		if(this.cart.length !== 0){
			this.cart.computeTotal();
			orders["products"] = this.cart.contents;
			orders["totalAmount"] = this.cart.totalAmount;
			this.orders.push(orders);
		}
		return this
	}

}


// let cart1 = new Cart("Cart1");
// console.log(cart1);

const prodA = new Product('soap',9.99)
console.log(prodA)

const prodB = new Product('candy',2)
console.log(prodB)